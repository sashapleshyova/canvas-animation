function Point(canvas) {
    this.x = 0;
    this.y = 0;
    this.radius = Math.floor(1 + Math.random() * (5 + 1 - 1));

    this.yCoefficient = Math.floor(Math.random() * (180 + 1))
    this.xCoefficient = Math.floor(Math.random() * (360 + 1));

    this.vector = {
        x: () => this.width * Math.abs(Math.sin((this.xCoefficient + this.tick) * Math.PI / 180)),
        y: () => this.height * Math.abs(Math.cos((this.yCoefficient + this.tick) * Math.PI / 180)),
    }

    this.width = canvas.width;
    this.height = canvas.height;

    this.tick = 1;


    this.draw = function (ctx) {
        this.tick += 0.05;
        this.x = this.vector.x();
        this.y = this.vector.y();
        ctx.beginPath();
        ctx.fillStyle = '#222222';
        ctx.ellipse(this.x, this.y, this.radius+5, this.radius+5, 0, 0, Math.PI * 4);
        ctx.fill();
        ctx.beginPath();
        ctx.fillStyle = '#ffffff';
        ctx.ellipse(this.x, this.y, this.radius, this.radius, 0, 0, Math.PI * 4);
        ctx.fill();
    }
}

const STROKE_STYLE = {
    50: '#009ec8',
    75: '#006279',
    100: '#002a37',
}

function draw(ctx, x1, y1, x2, y2, dist) {
    console.log(dist);
    ctx.strokeStyle = STROKE_STYLE[dist];
    ctx.beginPath();
    ctx.moveTo(x1, y1);
    ctx.lineTo(x2, y2);
    ctx.stroke();
}

let canvas = document.getElementById("canvas"),
    ctx = canvas.getContext('2d');

canvas.height = document.documentElement.offsetHeight;
canvas.width = document.documentElement.offsetWidth;


const STAR_COUNT = 100;
let stars = [];

for (let i = 0; i < STAR_COUNT; i++) {
    stars.push(new Point(canvas))
}

setInterval(function () {
    ctx.fillStyle = 'rgba(0,0,0,0.5)';
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    stars.forEach(star => {
        star.draw(ctx);
    })
    for (let i = 0; i < STAR_COUNT; i++) {
        for (let j = i + 1; j < STAR_COUNT; j++) {
            if (Math.abs(stars[i].x - stars[j].x) < 101 && Math.abs(stars[i].y - stars[j].y) < 101) {
                let dist = (Math.abs(stars[i].x - stars[j].x) + Math.abs(stars[i].y - stars[j].y))/2;
                draw(ctx, stars[i].x, stars[i].y, stars[j].x, stars[j].y, dist < 51 ? 50 : dist < 76 ? 75 : dist<101 ? 100 : null)
            }
        }
    }
}, 100)

